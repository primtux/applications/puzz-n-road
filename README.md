# Puzz n road

 Puzz'n Road est un jeu de logique. Une fois un défi sélectionné,
 le but du jeu consiste à disposer les neuf pièces de puzzle sur le plateau de jeu.
  Le réseau de chemins ainsi créé permet aux cibles dessinées sur le pourtour
  du plateau de se rejoindre ou non.

  https://numerique53.ac-nantes.fr/ressources/applications/puzznroad.html